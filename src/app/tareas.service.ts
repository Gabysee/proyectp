import { Injectable } from '@angular/core';
import { Tareas } from './tareas';

@Injectable()
export class TareasService {
  data: Tareas[];
  constructor() { 
    this.data = JSON.parse(localStorage.getItem('tareas') || '[]');
}

read() {
  this.data = JSON.parse(localStorage.getItem('tareas') || '[]');
  return this.data;
}

save(data: Tareas[]) {
  debugger;
  this.data = data;
  localStorage.setItem('tareas', JSON.stringify(this.data));
}

findById(id: String) {
  return this.data.find(x => x.id === id);
}
}

