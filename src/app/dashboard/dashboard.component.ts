import { Component, OnInit } from '@angular/core';
import { Dashboard } from '../dashboard';
import { DashboardService } from '../dashboard.service';
import {Proyectos} from '../proyectos';
import {Tareas} from '../tareas';
import {ProyectosService } from '../proyectos.service';
import {EstadosService } from '../estados.service';
import {TareasService } from '../tareas.service';
import {ActivatedRoute} from '@angular/router';
import { Estados } from '../estados';



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {

    proyectos:Proyectos;
    tareas:Tareas;
    estados:Estados;
    private sub: any;
    private data: any;
    private task: any;
     id:string;
    constructor(private route: ActivatedRoute,
                private service: ProyectosService,
                private gs: EstadosService,
                private ts: TareasService,) {}

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {this.proyectos = this.service.findById(params['id']);});
    
    this.data = this.gs.read();
    this.task = this.ts.read();
    //this.data = this.route.params.subscribe(params => {this.estados = this.service.findById(params['id']);});
}
}
