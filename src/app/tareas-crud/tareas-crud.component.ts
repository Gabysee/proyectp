import { Component, OnInit } from '@angular/core';
import { Tareas } from '../tareas';
import { TareasService } from '../tareas.service';
import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-tareas-crud',
  templateUrl: './tareas-crud.component.html',
  styleUrls: ['./tareas-crud.component.css']
})
export class TareasCrudComponent implements OnInit {
  
  data: Tareas[];
  current_tareas: Tareas;
    tareas: Tareas;
    id:string;
    sub:any;
    crud_operation = { is_new: false, is_visible: false };
    constructor(private service: TareasService,
      private route: ActivatedRoute,
    private router: Router  )  {
  }
  

  ngOnInit() {
    this.current_tareas = new Tareas();
    this.sub = this.route.params.subscribe(params => {this.id = params['id']});
    this.crud_operation.is_visible = true;
    this.crud_operation.is_new = true;
    //this.current_tareas = this.route.params.subscribe(params => {this.tareas = this.service.findById(params['id']);});
   //this.data = this.service.read();
    //this.current_tareas = new Tareas();
  }

  new() {
    this.current_tareas = new Tareas();
    
    this.id = this.route.params['id'];
    this.crud_operation.is_visible = true;
    this.crud_operation.is_new = true;
  }

  edit(row) {
    this.crud_operation.is_visible = true;
    this.crud_operation.is_new = false;
    this.current_tareas = row;
  }

  delete(row) {
    this.crud_operation.is_new = false;
    const index = this.data.indexOf(row, 0);
    if (index > -1) {
      this.data.splice(index, 1);
    }
    this.save();
  }

  save() {
    if (this.crud_operation.is_new) {
      this.data.push(this.current_tareas);

      //this.router.navigate(['./proyectos']);
      //this.router.navigate(['/heroes', { id: heroId }]);
    }
    debugger;
    this.service.save(this.data);
    this.current_tareas = new Tareas();
    this.crud_operation.is_visible = false;
  }

}
